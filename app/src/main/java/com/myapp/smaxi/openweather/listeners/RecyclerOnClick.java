package com.myapp.smaxi.openweather.listeners;

import android.view.View;

/**
 * Created by Smaxi on 6/1/2017.
 */

public interface RecyclerOnClick {
    //MY CUSTOM IMPLEMENTATION OF CLICK FOR RECYCLEVIEW
    void onClick(View view, int position);
    void onLongClick(View view, int position);
}



