package com.myapp.smaxi.openweather.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.myapp.smaxi.openweather.R;
import com.myapp.smaxi.openweather.classes.weather;
import com.myapp.smaxi.openweather.databinding.AdapterWeatherlistBinding;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Smaxi on 5/31/2017.
 */

public class rAdapter_weather extends RecyclerView.Adapter<rAdapter_weather.ViewHolder>{

    private static String IMG_URL = "http://openweathermap.org/img/w/%s.png"; //URL TO GET IMAGE OF WEATHER
    public ArrayList<weather> weatherList;
    Context context;
    public int count = 0;

    public rAdapter_weather(Context context, ArrayList<weather> weatherList) {
        if(weatherList.size() > 10){
            count = 10;
        }
        else{
            count = weatherList.size();
        }
        this.context = context;
        this.weatherList = new ArrayList<weather>();
        this.weatherList.addAll(weatherList);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView icon;
        public TextView city;
        public TextView weather;
        public TextView temp;
        public AdapterWeatherlistBinding binder;
        public ViewHolder(View v) {
            super(v);
            binder = DataBindingUtil.bind(v);
            icon = binder.weathericon;
            city = binder.city;
            weather = binder.weather;
            temp = binder.temperature;
        }
    }

    @Override
    public rAdapter_weather.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_weatherlist, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(rAdapter_weather.ViewHolder holder, int position) {
        Picasso.with(context).load(String.format(IMG_URL, weatherList.get(position).getIcon())).into(holder.icon);
        holder.city.setText(weatherList.get(position).getCity());
        holder.weather.setText(weatherList.get(position).getMain_desc() + "(" + weatherList.get(position).getDesc() + ")");
        holder.temp.setText(weatherList.get(position).getTemp());
    }

    @Override
    public int getItemCount() {
        return count;
    }

    //UPDATE DATA LIST OF ADAPTER
    public void reload(ArrayList<weather> weatherList) {
        this.weatherList.clear();
        this.weatherList.addAll(weatherList);
        if(this.weatherList.size() > 10){
            count = 10;
        }
        else{
            count = this.weatherList.size();
        }
        notifyDataSetChanged();
    }
}
