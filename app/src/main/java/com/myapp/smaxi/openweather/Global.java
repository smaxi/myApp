package com.myapp.smaxi.openweather;

import android.content.Context;
import android.content.ContextWrapper;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.File;

/**
 * Created by Smaxi on 7/12/2017.
 */

public class Global {

    //GET STORAGE LOCATION
    public static File getDataStorage(Context context){
        ContextWrapper cw = new ContextWrapper(context);
        return cw.getFilesDir();
    }

    //CHECK INTERNET CONNECTIVTY
    public static boolean isConnectedToInternet(Context context){
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {return false;}
        return ni.isConnected();
    }
}
