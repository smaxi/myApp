package com.myapp.smaxi.openweather;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.myapp.smaxi.openweather.database.weatherDB;
import com.myapp.smaxi.openweather.databinding.LayoutWeatherBinding;
import com.myapp.smaxi.openweather.fragments.fragment_weatherlist_dev;
import com.myapp.smaxi.openweather.settings.mode;

public class weather_main extends FragmentActivity {

    private LayoutWeatherBinding binding;
    private weatherDB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.layout_weather);
        if(savedInstanceState == null) {
            //CALL THE FIRST FRAGMENT TO THE CONTAINER
            getFragmentManager().beginTransaction()
                    .add(R.id.fragmentContainer, new fragment_weatherlist_dev(), "fragment_weatherlist")
                    .commit();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        //INIT DATABASE
        db = weatherDB.getInstance(weather_main.this);
        //OPEN DATABASE
        db.open();

    }

    @Override
    protected void onPause() {
        super.onPause();
        //CLOSE DATABASE (OPPOSITE OF RESUME)
        db.close();
    }

    @SuppressWarnings("ResourceType")
    @Override
    public void onBackPressed(){
        fragment_weatherlist_dev x = (fragment_weatherlist_dev) getFragmentManager().findFragmentByTag("fragment_weatherlist");
        if (x != null && x.isVisible()) {
            AlertDialog.Builder localBuilder = new AlertDialog.Builder(weather_main.this);
            localBuilder.setMessage("Are you sure you want to exit?");
            localBuilder.setCancelable(true);
            localBuilder.setPositiveButton("Exit", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt){weather_main.this.finish();}});
            localBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt){paramAnonymousDialogInterface.cancel();}});
            localBuilder.create().show();
        }
        else{
            //MAKE SURE TO GO BACK TO THE PREVIOUS FRAGMENT
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.enter_left, R.anim.exit_right)
                    .replace(R.id.fragmentContainer, new fragment_weatherlist_dev(), "fragment_weatherlist")
                    .commit();
        }
    }
}
