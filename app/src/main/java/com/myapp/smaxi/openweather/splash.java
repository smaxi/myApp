package com.myapp.smaxi.openweather;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.myapp.smaxi.openweather.databinding.LayoutSplashBinding;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

/**
 * Created by Smaxi on 7/12/2017.
 */

public class splash extends Activity {

    private static int SPLASH_TIME_OUT = 2000; //2 SECONDS
    Disposable disposable;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final LayoutSplashBinding splashbinding = DataBindingUtil.setContentView(this, R.layout.layout_splash);
        doAnimationObserver(splashbinding);
        initActivation();
    }

    //ANIMATION OF LOGO
    private void doAnimationObserver(LayoutSplashBinding splashbinding){
        disposable = Observable.just(splashbinding.logo)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(values -> {values.setVisibility(View.VISIBLE); values.startAnimation(AnimationUtils.loadAnimation(splash.this, R.anim.fadein)); });
        //.subscribe(values -> {values.startAnimation(AnimationUtils.loadAnimation(splash.this, R.anim.fadein)); values.setVisibility(View.VISIBLE);},
        //            error -> Log.e("TAG", "Error: " + error.getMessage()),() -> System.out.println("SMAXI"));
    }

    private void initActivation(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(splash.this, weather_main.class);
                startActivity(intent);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(disposable!=null && !disposable.isDisposed()){
            disposable.dispose();
        }
    }
}
