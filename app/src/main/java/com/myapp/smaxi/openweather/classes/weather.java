package com.myapp.smaxi.openweather.classes;

import java.io.Serializable;

/**
 * Created by Smaxi on 7/12/2017.
 */

public class weather implements Serializable {

    private String city_id;
    private String city;
    private String country;
    private String weather_id;
    private String main_desc;
    private String desc;
    private String icon;
    private String temp;
    private String pressure;
    private String humid;
    private String tempmin;
    private String tempmax;
    private String windspeed;
    private String winddir;
    private String cloud;
    private String sunrise;
    private String sunset;

    public weather(String city_id, String city, String country, String weather_id, String main_desc, String desc, String icon, String temp, String pressure, String humid, String tempmin, String tempmax, String windspeed
        ,String winddir, String cloud, String sunrise, String sunset){
        this.city_id = city_id;
        this.city = city;
        this.country = country;
        this.weather_id = weather_id;
        this.main_desc = main_desc;
        this.desc = desc;
        this.icon = icon;
        this.temp = temp;
        this.pressure = pressure;
        this.humid = humid;
        this.tempmin = tempmin;
        this.tempmax = tempmax;
        this.windspeed = windspeed;
        this.winddir = winddir;
        this.cloud = cloud;
        this.sunrise = sunrise;
        this.sunset = sunset;
    }


    //SETTER
    public void setCity_id(String city_id){
        this.city_id = city_id;
    }
    public void setCity(String city){
        this.city = city;
    }
    public void setCountry(String country){
        this.country = country;
    }
    public void setWeather_id(String weather_id){
        this.weather_id = weather_id;
    }
    public void setMain_desc(String main_desc){
        this.main_desc = main_desc;
    }
    public void setDesc(String desc){
        this.desc = desc;
    }
    public void setIcon(String icon){
        this.icon = icon;
    }
    public void setTemp(String temp){
        this.temp = temp;
    }
    public void setPressure(String pressure){
        this.pressure = pressure;
    }
    public void setHumid(String humid){
        this.humid = humid;
    }
    public void setTempmin(String tempmin){
        this.tempmin = tempmin;
    }
    public void setTempmax(String tempmax){
        this.tempmax = tempmax;
    }
    public void setWindspeed(String windspeed){
        this.windspeed = windspeed;
    }
    public void setWinddir(String winddir){
        this.winddir = winddir;
    }
    public void setCloud(String cloud){
        this.city = city;
    }
    public void setSunrise(String sunrise){
        this.sunrise = sunrise;
    }
    public void setSunset(String sunset){
        this.sunset = sunset;
    }

    //GETTER
    public String getCity_id(){
        return this.city_id;
    }
    public String getCity(){
        return this.city;
    }
    public String getCountry(){
        return this.country;
    }
    public String getWeather_id(){
        return this.weather_id;
    }
    public String getMain_desc(){
        return this.main_desc;
    }
    public String getDesc(){
        return this.desc;
    }
    public String getIcon(){
        return this.icon;
    }
    public String getTemp(){
        return this.temp;
    }
    public String getPressure(){
        return this.pressure;
    }
    public String getHumid(){
        return this.humid;
    }
    public String getTempmin(){
        return this.tempmin;
    }
    public String getTempmax(){
        return this.tempmax;
    }
    public String getWindspeed(){
        return this.windspeed;
    }
    public String getWinddir(){
        return this.winddir;
    }
    public String getCloud(){
        return this.cloud;
    }
    public String getSunrise(){
        return this.sunrise;
    }
    public String getSunset(){
        return this.sunset;
    }
}
