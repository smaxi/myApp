package com.myapp.smaxi.openweather.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.myapp.smaxi.openweather.classes.weather;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * Created by Smaxi on 7/12/2017.
 */

public class weatherDB extends SQLiteOpenHelper {

    private static weatherDB sInstance;

    private Context weather_context;
    private static String DB_NAME = "weather.sqlite3";
    private static int DB_VERSION = 1;
    private static String DB_PATH ="";
    public SQLiteDatabase myDataBase;

    //GET ONLY ONE INSTANCE FROM OPENING OF DATABASE
    public static synchronized weatherDB getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new weatherDB(context.getApplicationContext());
        }
        return sInstance;
    }

    private weatherDB(Context context){
        super(context, DB_NAME, null, DB_VERSION);
        this.weather_context = context;
        DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        boolean dbexist = checkDatabase();
        if (dbexist){}
        else{
            createDatabase();
        }
    }

    public void createDatabase(){
        boolean dbexist = checkDatabase();
        if (dbexist){}
        else {
            this.getReadableDatabase();
            try {
                copydatabase();
            }
            catch (IOException e){ throw new Error("Error copying database"); }
        }
    }

    private boolean checkDatabase() {
        SQLiteDatabase checkDB = null;
        try {
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
        }catch (SQLiteException e) {}
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }

    private void copydatabase() throws IOException {
        InputStream myinput = weather_context.getAssets().open(DB_NAME);
        String outfilename = DB_PATH + DB_NAME;
        OutputStream myoutput = new FileOutputStream(outfilename);

        byte[] buffer = new byte[4096];
        int length;
        while ((length = myinput.read(buffer)) > 0) {
            myoutput.write(buffer, 0, length);
        }

        myoutput.flush();
        myoutput.close();
        myinput.close();
    }


    //OPEN CLOSE
    public void open(){
        myDataBase = this.getWritableDatabase();
    }

    public synchronized void close() {
        if (myDataBase != null){
            myDataBase.close();
        }
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {}

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}


    //MAIN FUNCTIONS
    public void BatchAdd(ArrayList<weather> weathers){
        boolean isDB = false;
        String sql = "INSERT INTO weather " + "(city_id, city, country, weather_id, main_desc, desc, icon, temp, pressure, humid, tempmin, tempmax, windspeed, winddir, cloud, sunrise, sunset)" +
                " VALUES" + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        SQLiteStatement statement = myDataBase.compileStatement(sql);
        String sql2 = "UPDATE weather SET city =?, country =?, weather_id =?, main_desc =?, desc =?, icon =?, temp =?, pressure =?, humid =?, tempmin =?, tempmax =?" +
                ", windspeed =?, winddir =?, cloud =?, sunrise =?, sunset =? WHERE city_id=?;";
        SQLiteStatement statement2 = myDataBase.compileStatement(sql2);
        myDataBase.beginTransaction();
        for(int x = 0 ; x < weathers.size(); x++){
            isDB = false;
            isDB = checkContent(weathers.get(x).getCity_id());
            //INSERT
            if(!isDB){
                statement.clearBindings();
                statement.bindString(1, weathers.get(x).getCity_id());
                statement.bindString(2, weathers.get(x).getCity());
                statement.bindString(3, weathers.get(x).getCountry());
                statement.bindString(4, weathers.get(x).getWeather_id());
                statement.bindString(5, weathers.get(x).getMain_desc());
                statement.bindString(6, weathers.get(x).getDesc());
                statement.bindString(7, weathers.get(x).getIcon());
                statement.bindString(8, weathers.get(x).getTemp());
                statement.bindString(9, weathers.get(x).getPressure());
                statement.bindString(10, weathers.get(x).getHumid());
                statement.bindString(11, weathers.get(x).getTempmin());
                statement.bindString(12, weathers.get(x).getTempmax());
                statement.bindString(13, weathers.get(x).getWindspeed());
                statement.bindString(14, weathers.get(x).getWinddir());
                statement.bindString(15, weathers.get(x).getCloud());
                statement.bindString(16, weathers.get(x).getSunrise());
                statement.bindString(17, weathers.get(x).getSunset());
                statement.execute();
            }
            //UPDATE IF THERES RECORD
            else{
                statement2.clearBindings();
                statement2.bindString(1, weathers.get(x).getCity());
                statement2.bindString(2, weathers.get(x).getCountry());
                statement2.bindString(3, weathers.get(x).getWeather_id());
                statement2.bindString(4, weathers.get(x).getMain_desc());
                statement2.bindString(5, weathers.get(x).getDesc());
                statement2.bindString(6, weathers.get(x).getIcon());
                statement2.bindString(7, weathers.get(x).getTemp());
                statement2.bindString(8, weathers.get(x).getPressure());
                statement2.bindString(9, weathers.get(x).getHumid());
                statement2.bindString(10, weathers.get(x).getTempmin());
                statement2.bindString(11, weathers.get(x).getTempmax());
                statement2.bindString(12, weathers.get(x).getWindspeed());
                statement2.bindString(13, weathers.get(x).getWinddir());
                statement2.bindString(14, weathers.get(x).getCloud());
                statement2.bindString(15, weathers.get(x).getSunrise());
                statement2.bindString(16, weathers.get(x).getSunset());
                statement2.bindString(17, weathers.get(x).getCity_id());
                statement2.execute();
            }
        }
        myDataBase.setTransactionSuccessful();
        myDataBase.endTransaction();
    }

    public ArrayList<weather> getList(){
        ArrayList<weather> weathers = new ArrayList<weather>();

        String query = "SELECT * FROM weather;";
        Cursor cursor = myDataBase.rawQuery(query, null);
        weather temp;

        while (cursor.moveToNext()) {
            temp = extractContent(cursor);
            weathers.add(temp);
        }
        cursor.close();
        return weathers;
    }

    public weather getWeather(String city_id){
        String query = "SELECT * FROM weather WHERE city_id="+city_id+";";
        Cursor cursor = myDataBase.rawQuery(query, null);
        try{
            if(cursor.getCount()!=0){
                cursor.moveToNext();
                return extractContent(cursor);
            }
            else{
                return new weather("0","404","","","","","","","","","","","","","","","");
            }
        }
        finally {
            cursor.close();
        }

    }

    //SUB FUNCTIONS
    private Boolean checkContent(String city_id){
        String query = "SELECT * FROM weather WHERE city_id='"+city_id+"';";
        Cursor cursor = myDataBase.rawQuery(query, null);
        boolean x = false;
        if(cursor.getCount()!=0){
            cursor.moveToNext();
            if(city_id.equals(cursor.getString(cursor.getColumnIndex("city_id")))){
                x = true;
            }
            else{
                x = false;
            }
        }
        else{

            x = false;
        }
        cursor.close();
        return x;
    }

    private weather extractContent(Cursor cursor){
        weather weather = new weather(cursor.getString(cursor.getColumnIndex("city_id")),
                cursor.getString(cursor.getColumnIndex("city")),
                cursor.getString(cursor.getColumnIndex("country")),
                cursor.getString(cursor.getColumnIndex("weather_id")),
                cursor.getString(cursor.getColumnIndex("main_desc")),
                cursor.getString(cursor.getColumnIndex("desc")),
                cursor.getString(cursor.getColumnIndex("icon")),
                cursor.getString(cursor.getColumnIndex("temp")),
                cursor.getString(cursor.getColumnIndex("pressure")),
                cursor.getString(cursor.getColumnIndex("humid")),
                cursor.getString(cursor.getColumnIndex("tempmin")),
                cursor.getString(cursor.getColumnIndex("tempmax")),
                cursor.getString(cursor.getColumnIndex("windspeed")),
                cursor.getString(cursor.getColumnIndex("winddir")),
                cursor.getString(cursor.getColumnIndex("cloud")),
                cursor.getString(cursor.getColumnIndex("sunrise")),
                cursor.getString(cursor.getColumnIndex("sunset")));
        return weather;
    }

}
