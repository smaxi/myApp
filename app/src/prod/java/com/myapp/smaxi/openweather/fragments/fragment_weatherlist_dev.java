package com.myapp.smaxi.openweather.fragments;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jakewharton.rxbinding2.view.RxView;
import com.myapp.smaxi.openweather.R;
import com.myapp.smaxi.openweather.adapters.rAdapter_weather;
import com.myapp.smaxi.openweather.classes.weather;
import com.myapp.smaxi.openweather.database.weatherDB;
import com.myapp.smaxi.openweather.databinding.FragmentWeatherlistBinding;
import com.myapp.smaxi.openweather.listeners.RecyclerOnClick;
import com.myapp.smaxi.openweather.listeners.RecyclerOnClickListener;
import com.myapp.smaxi.openweather.services.weatherListService_dev;

import java.util.ArrayList;

import io.reactivex.disposables.Disposable;

/**
 * Created by Smaxi on 7/12/2017.
 */

public class fragment_weatherlist_dev extends Fragment {

    private FragmentWeatherlistBinding binding;

    private RecyclerView weatherRecyclerView;
    private LinearLayoutManager layoutManager;
    private rAdapter_weather weatherAdapter;

    private Button reload;
    private Disposable reloadDisposable;

    private MyReceiver receiver;

    public String[] locations = {"2643743", "3067696" , "5391959"};

    public static fragment_weatherlist_dev newInstance(String s) {
        fragment_weatherlist_dev fragment = new fragment_weatherlist_dev();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_weatherlist, container, false);
        receiver = new MyReceiver();
        IntentFilter filter = new IntentFilter(MyReceiver.ACTION_RESP);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        getActivity().getApplicationContext().registerReceiver(receiver, filter);
        setLayoutBindings();
        setLayoutListeners();
        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        setList();
    }

    private void setLayoutBindings(){
        this.reload = binding.reload;
    }

    private void setLayoutListeners(){
        reloadDisposable = RxView.clicks(reload).subscribe(a -> reload());
    }

    private void reload(){
        Intent intent = new Intent(getActivity().getApplicationContext(), weatherListService_dev.class);
        intent.putExtra("location", locations);
        getActivity().getApplicationContext().startService(intent);
    }

    private void setList(){
        weatherDB db = weatherDB.getInstance(getActivity().getApplicationContext());
        System.out.println("SMAXI LOG DB "+ db.getList().size());
        weatherRecyclerView = binding.myRecycleView;
        weatherAdapter = new rAdapter_weather(getActivity().getApplicationContext(), db.getList());
        layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        weatherRecyclerView.setLayoutManager(layoutManager);
        weatherRecyclerView.setItemAnimator(new DefaultItemAnimator());
        weatherRecyclerView.setAdapter(weatherAdapter);
        weatherRecyclerView.addOnItemTouchListener(new RecyclerOnClickListener(getActivity().getApplicationContext(), weatherRecyclerView, new RecyclerOnClick() {
            @Override
            public void onClick(View view, int position) {
                Fragment details = new fragment_weatherdetails_dev();
                Bundle bundle = new Bundle();
                bundle.putSerializable("details", db.getList().get(position));
                details.setArguments(bundle);
                getFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.enter_right, R.anim.exit_left)
                        .replace(R.id.fragmentContainer, details,"fragment_details")
                        .commit();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void onPause() {
        super.onPause();
        if(reloadDisposable!=null && !reloadDisposable.isDisposed()){
            reloadDisposable.dispose();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(receiver != null){
            getActivity().getApplicationContext().unregisterReceiver(receiver);
        }
    }

    public class MyReceiver extends BroadcastReceiver {

        public static final String ACTION_RESP = "openweatherapp.RECEIVE_WEATHER_UPDATE";
        public MyReceiver() {}

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle args = intent.getBundleExtra("BUNDLE");
            ArrayList<weather> temp = (ArrayList<weather>) args.getSerializable("updatedItem");
            boolean result = intent.getBooleanExtra("result", false);
            System.out.println("SMAXI LOG received");
            if(result){
                System.out.println("SMAXI LOG DB batchadd");
                weatherDB db = weatherDB.getInstance(getActivity().getApplicationContext());
                db.BatchAdd(temp);
                weatherAdapter.reload(db.getList());
            }
        }
    }
}
