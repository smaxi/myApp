package com.myapp.smaxi.openweather.fragments;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jakewharton.rxbinding2.view.RxView;
import com.myapp.smaxi.openweather.R;
import com.myapp.smaxi.openweather.classes.weather;
import com.myapp.smaxi.openweather.database.weatherDB;
import com.myapp.smaxi.openweather.databinding.FragmentWeatherdetailsBinding;
import com.myapp.smaxi.openweather.services.weatherDetailService_dev;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import io.reactivex.disposables.Disposable;

/**
 * Created by Smaxi on 7/12/2017.
 */

public class fragment_weatherdetails_dev extends Fragment {

    private static String IMG_URL = "http://openweathermap.org/img/w/%s.png";
    private FragmentWeatherdetailsBinding binding;
    private Button refresh;
    private Disposable reloadDisposable;

    private MyDetailReceiver receiver;

    public static fragment_weatherdetails_dev newInstance(String s) {
        fragment_weatherdetails_dev fragment = new fragment_weatherdetails_dev();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_weatherdetails, container, false);
        receiver = new MyDetailReceiver();
        IntentFilter filter = new IntentFilter(MyDetailReceiver.ACTION_RESP);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        getActivity().getApplicationContext().registerReceiver(receiver, filter);
        setLayoutBindings();
        setLayoutListeners();
        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        setData(((weather) getArguments().getSerializable("details")).getCity_id());
    }

    private void setLayoutBindings(){
        this.refresh = binding.reload;
    }

    private void setLayoutListeners(){
        reloadDisposable = RxView.clicks(refresh).subscribe(a -> reload());
    }

    private void reload(){
        Bundle args = getArguments();
        weather details = (weather) args.getSerializable("details");
        Intent intent = new Intent(getActivity().getApplicationContext(), weatherDetailService_dev.class);
        intent.putExtra("location", details.getCity_id());
        getActivity().getApplicationContext().startService(intent);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(reloadDisposable!=null && !reloadDisposable.isDisposed()){
            reloadDisposable.dispose();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(receiver != null){
            getActivity().getApplicationContext().unregisterReceiver(receiver);
        }
    }

    private void setData(String city_id){
        weatherDB db = weatherDB.getInstance(getActivity().getApplicationContext());
        db.getWeather(city_id);
        Bundle args = getArguments();
        weather details = (weather) args.getSerializable("details");
        binding.cityName.setText(details.getCity() + "," + details.getCountry());
        Picasso.with(getActivity().getApplicationContext()).load(String.format(IMG_URL, details.getIcon())).into(binding.weatherIcon);
        binding.weatherDescription.setText(details.getMain_desc() + "(" + details.getDesc() + ")");
        binding.actualTemp.setText(details.getTemp() + "C");
        binding.minimumTemp.setText("min: " + details.getTempmin() + " C");
        binding.maxTemp.setText("max: " + details.getTempmax() + " C");
        binding.windspeed.setText(details.getWindspeed() + " m/s");
        binding.winddirection.setText(details.getWinddir() + " degrees");
        binding.cloudiness.setText(details.getCloud() + " %");
        binding.humidity.setText(details.getHumid() + " %");
        binding.pressure.setText(details.getPressure() + " hPa");
        binding.sunrise.setText(details.getSunrise());
        binding.sunset.setText(details.getSunset());
    }

    public class MyDetailReceiver extends BroadcastReceiver {

        public static final String ACTION_RESP = "openweatherapp.RECEIVE_WEATHER_DETAIL_UPDATE_DEV";
        public MyDetailReceiver() {}

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle args = intent.getBundleExtra("BUNDLE");
            weather temp = (weather) args.getSerializable("updatedItem");
            boolean result = intent.getBooleanExtra("result", false);
            if(result){
                ArrayList<weather> list = new ArrayList<>();
                list.add(temp);
                weatherDB db = weatherDB.getInstance(getActivity().getApplicationContext());
                db.BatchAdd(list);
                setData(((weather) getArguments().getSerializable("details")).getCity_id());
            }
        }
    }

}
