package com.myapp.smaxi.openweather.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.myapp.smaxi.openweather.Global;
import com.myapp.smaxi.openweather.classes.weather;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Smaxi on 7/12/2017.
 */

public class weatherDetailService_dev extends IntentService{

    private static final String ACTION_RESP = "openweatherapp.RECEIVE_WEATHER_DETAIL_UPDATE_DEV";

    //USE ACTUAL DATA BUT FOR TESTING
    private static String BASE_URL = "http://api.openweathermap.org/data/2.5/weather?id=%s&units=metric&appid=%s";
    private static String apikey = "075c8258ab5005ff9ffc2c245ff16e14";

    private boolean result = false;
    public weatherDetailService_dev() {
        super("SMA");
    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        System.out.println("SMAXI LOG Service Start EXP MODE");
        Intent broadcastIntent = new Intent();
        ArrayList<weather> list = new ArrayList<>();
        Bundle bundle = new Bundle();
        broadcastIntent.setAction(ACTION_RESP);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        if(Global.isConnectedToInternet(this)){
            System.out.println("SMAXI LOG Service Start Internet Connected EXP MODE");
            String tempLocation = intent.getStringExtra("location");
            System.out.println("SMAXI LOG EXP MODE" + tempLocation);
            JSONObject temp = getWeatherUpdate(tempLocation);
            if(temp != null){
                result = false;
            }
            result = true;
            broadcastIntent.putExtra("result", result);
            bundle.putSerializable("updatedItem", JSONweatherParser(temp));
        }
        else{
            System.out.println("SMAXI LOG Service Start no Internet EXP MODE");
            broadcastIntent.putExtra("result", result);
            bundle.putSerializable("updatedItem", new weather("0", "404","","","","","","","","","","","","","","",""));
        }
        broadcastIntent.putExtra("BUNDLE", bundle);
        sendBroadcast(broadcastIntent);
        System.out.println("SMAXI end service EXP MODE");
    }

    private weather JSONweatherParser(JSONObject weather){
        System.out.println("SMAXI LOG Start Parser EXP MODE");

        try {
            System.out.println("SMAXI LOG Check ID EXP MODE " + weather.getJSONArray("weather").getJSONObject(0).getString("id"));
            DateFormat df = DateFormat.getDateTimeInstance();
            return new weather(weather.getString("id"), weather.getString("name"), weather.getJSONObject("sys").getString("country"),
                    weather.getJSONArray("weather").getJSONObject(0).getString("id"), weather.getJSONArray("weather").getJSONObject(0).getString("main"),
                    weather.getJSONArray("weather").getJSONObject(0).getString("description"), weather.getJSONArray("weather").getJSONObject(0).getString("icon"), weather.getJSONObject("main").getString("temp"),
                    weather.getJSONObject("main").getString("pressure"),weather.getJSONObject("main").getString("humidity"),
                    weather.getJSONObject("main").getString("temp_min"),weather.getJSONObject("main").getString("temp_max"),
                    weather.getJSONObject("wind").getString("speed"),weather.getJSONObject("wind").getString("deg"),
                    weather.getJSONObject("clouds").getString("all"),
                    df.format(new Date(weather.getJSONObject("sys").getLong("sunrise")*1000)),df.format(new Date(weather.getJSONObject("sys").getLong("sunset")*1000)));
        } catch (JSONException e) {
            return new weather("0", "404","","","","","","","","","","","","","","","");
        }
    }

    private JSONObject getWeatherUpdate(String city_id){
        System.out.println("SMAXI LOG get connection EXP MODE");
        try {
            URL url = new URL(String.format(BASE_URL,city_id,apikey));
            HttpURLConnection connection =(HttpURLConnection)url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuffer json = new StringBuffer(1024);
            String tmp="";
            while((tmp=reader.readLine())!=null)
                json.append(tmp).append("\n");
            reader.close();
            JSONObject data = new JSONObject(json.toString());
            if(data.getInt("cod") != 200){
                return null;
            }
            System.out.println("SMAXI LOG connection success EXP MODE");
            return data;
        }catch(Exception e){
            return null;
        }
    }
}
